/*
 * Copyright 2013 David Salter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.apps.todo.web;

import com.davidsalter.apps.todo.ejb.ToDoItemController;
import com.davidsalter.apps.todo.model.ToDoItem;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import junit.framework.Assert;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * 
 * @author David Salter david@davidsalter.com
 */
@RunWith(Arquillian.class)
public class ToDoBeanTest {

    @Inject
    ToDoBean toDoBean;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    
    private static final String[] ITEM_DESCRIPTIONS = {
        "Do this",
        "Do that",
        "Have a cup of tea"
    };

    public ToDoBeanTest() {
    }

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(ToDoItem.class.getPackage())
                .addPackage(ToDoBean.class.getPackage())
                .addPackage(ToDoItemController.class.getPackage())
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    /**
     * Insert so test data into the database before each test is run.
     *
     * @throws Exception
     */
    @Before
    public void prepareTest() throws Exception {
        utx.begin();
        for (String title : ITEM_DESCRIPTIONS) {
            ToDoItem item = new ToDoItem(title);
            em.persist(item);
        }
        utx.commit();
    }

    /**
     * Deletes all of the test data in the ToDoItem table after each test has
     * run.
     *
     * @throws Exception
     */
    @After
    public void commitTransaction() throws Exception {
        utx.begin();
        em.createQuery("delete from ToDoItem").executeUpdate();
        utx.commit();

        toDoBean.setToDelete(null);
    }

    @Test
    public void testGetItems() {
        List<ToDoItem> items = toDoBean.getItems();
        Assert.assertEquals("Invalid number of items returned", ITEM_DESCRIPTIONS.length, items.size());
    }

    @Test
    public void testGetTimezone() {
        TimeZone tz = toDoBean.getTimeZone();
        TimeZone localTZ = TimeZone.getDefault();
        Assert.assertEquals("Invalid TimeZone returned", localTZ.getDisplayName(), tz.getDisplayName());
    }

    @Test
    public void testGetLastRefresh() {
        toDoBean.getItems();
        Date lastRefresh = toDoBean.getLastRefresh();
        Assert.assertNotNull("Last refresh date is null", lastRefresh);
    }

    @Test
    public void testDeleteItem() {
        String expectedReturn = "confirm";

        Assert.assertNull(toDoBean.getToDelete());

        ToDoItem item = new ToDoItem();
        String returnValue = toDoBean.deleteItem(item);
        Assert.assertSame("Item to be deleted is not set correctly", item, toDoBean.getToDelete());
        Assert.assertEquals("Return path is incorrectly set", expectedReturn, returnValue);

    }

    
    @Test
    public void testAddConfirmedItem() {
        String expectedReturn = "items";

        String returnValue = toDoBean.addConfirmedItem();
        Assert.assertEquals("Return path is incorrectly set", expectedReturn, returnValue);
    }

    
    @Test
    public void testDeleteConfirmedItem() {
        String expectedReturn = "items";

        String returnValue = toDoBean.deleteConfirmedItem();
        Assert.assertEquals("Return path is incorrectly set", expectedReturn, returnValue);
    }

    @Test
    public void testCancelDeleteConfirmedItem() {
        String expectedReturn = "confirm";

        Assert.assertNull(toDoBean.getToDelete());

        ToDoItem item = new ToDoItem();
        String returnValue = toDoBean.deleteItem(item);
        Assert.assertSame("Item to be deleted is not set correctly", item, toDoBean.getToDelete());
        Assert.assertEquals("Return path is incorrectly set", expectedReturn, returnValue);
        Assert.assertNotNull(toDoBean.getToDelete());

        expectedReturn = "items";
        returnValue = toDoBean.cancelDeleteConfirmedItem();
        Assert.assertEquals("Return path is incorrectly set", expectedReturn, returnValue);
        Assert.assertNull(toDoBean.getToDelete());

    }

    @Test
    public void testEditItem() {
        String expectedReturn = "edit";

        Assert.assertNotNull(toDoBean.getItem());

        ToDoItem item = new ToDoItem();
        String returnValue = toDoBean.editItem(item);
        Assert.assertSame("Item to be edited is not set correctly", item, toDoBean.getItem());
        Assert.assertEquals("Return path is incorrectly set", expectedReturn, returnValue);
    }

    @Test
    public void testEditConfirmedItem() {
        String expectedReturn = "items";

        String returnValue = toDoBean.editConfirmedItem();
        Assert.assertEquals("Return path is incorrectly set", expectedReturn, returnValue);
    }

    
    @Test
    public void testCancelConfirmedItem() {
        String expectedReturn = "items";

        String returnValue = toDoBean.cancelConfirmedItem();
        Assert.assertEquals("Returned path is incorrectly set", expectedReturn, returnValue);
        Assert.assertNotNull(toDoBean.getItem());
    }
}