/*
 * Copyright 2013 David Salter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.apps.todo.ejb;

import com.davidsalter.apps.todo.model.ToDoItem;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import junit.framework.Assert;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Arquillian test class to test the ToDoItemControllerBean
 * 
 * @author David Salter david@davidsalter.com
 */
@RunWith(Arquillian.class)
public class ToDoItemControllerBeanTest {

    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @EJB
    ToDoItemController toDoItemController;

    /**
     * Create the archive for Arquillian testing.
     * @return The archive
     */
    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(ToDoItem.class.getPackage())
                .addPackage(ToDoItemController.class.getPackage())
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }
    private static final String[] ITEM_DESCRIPTIONS = {
        "Do this",
        "Do that",
        "Have a cup of tea"
    };

    /**
     * Default constructor
     */
    public ToDoItemControllerBeanTest() {
    }

    /**
     * Insert so test data into the database before each test is run.
     * @throws Exception
     */
    @Before
    public void prepareTest() throws Exception {
        utx.begin();
        for (String title : ITEM_DESCRIPTIONS) {
            ToDoItem item = new ToDoItem(title);
            em.persist(item);
        }
        utx.commit();
    }

    /**
     * Deletes ass of the test data in the ToDoItem table after each test has run.
     * @throws Exception
     */
    @After
    public void commitTransaction() throws Exception {
        utx.begin();
        em.createQuery("delete from ToDoItem").executeUpdate();
        utx.commit();
    }

    /**
     * Test the ToDoItemController.getItems() method.
     */
    @Test
    public void testGetItems() {
        List<ToDoItem> items = toDoItemController.getItems();
        Assert.assertEquals("Invalid number of items returned", ITEM_DESCRIPTIONS.length, items.size());
    }

    /**
     * Test the ToDoItemController.addItem() method.
     */
    @Test
    public void testAddItem() {
        ToDoItem item = new ToDoItem("A test item");
        toDoItemController.addItem(item);
        List<ToDoItem> items = toDoItemController.getItems();
        Assert.assertEquals("Invalid number of items returned after adding an item", ITEM_DESCRIPTIONS.length + 1, items.size());
    }

    /**
     * Test the ToDoItemController.deleteItem() method.
     */
    @Test
    public void testDeleteItem() {
        List<ToDoItem> items = toDoItemController.getItems();

        toDoItemController.deleteItem(items.get(0));

        items = toDoItemController.getItems();
        Assert.assertEquals("Invalid number of items returned after deleting an item", ITEM_DESCRIPTIONS.length - 1, items.size());
    }

    /**
     * Test the ToDoItemController.editItem() method.
     */
    @Test
    public void testEditItem() {
        ToDoItem item = new ToDoItem("A test item");
        toDoItemController.addItem(item);
        Integer id = item.getId();

        String newDescription = "A new description";
        item.setDescription(newDescription);
        toDoItemController.editItem(item);
        Assert.assertEquals("Invalid edit of item", newDescription, item.getDescription());

        ToDoItem copyItem = toDoItemController.getItem(id);
        Assert.assertEquals("Invalid edit of item", newDescription, copyItem.getDescription());
    }

    /**
     * Test the ToDoItemController.getItem() method.
     */
    @Test
    public void testGetItem() {

        List<ToDoItem> items = toDoItemController.getItems();
        for (ToDoItem item : items) {
            ToDoItem fromDatabase = toDoItemController.getItem(item.getId());
            Assert.assertEquals("Invalid id retrieved", fromDatabase.getId(), item.getId());
            Assert.assertEquals("Invalid description retrieved", fromDatabase.getDescription(), item.getDescription());
        }
    }
}