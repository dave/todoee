/*
 * Copyright 2013 David Salter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.apps.todo.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * 
 * @author David Salter david@davidsalter.com
 */
public class ToDoItemTest {

    public ToDoItemTest() {
    }

    @Test
    public void testGettersAndSetters() {
        final int id = 77;
        final String description = "A description";
        
        ToDoItem item = new ToDoItem();
        item.setDescription(description);
        item.setId(id);

        assertEquals("Description is not set correctly", description, item.getDescription());
        assertEquals("ID is not set correctly", id, item.getId().intValue());
    }

    @Test
    public void testConstructor() {
        final String description = "A description";
        
        ToDoItem item = new ToDoItem(description);

        assertEquals("Description is not set correctly", description, item.getDescription());
    }
}