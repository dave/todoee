/*
 * Copyright 2013 David Salter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.apps.todo.model;

import java.util.Properties;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import junit.framework.Assert;
import org.apache.openejb.BeanContext;
import org.apache.openejb.bval.BeanValidationAppendixInterceptor;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * jUnit tests to ensure that Bean Validation on the ToDoItem class is successful.
 * 
 * @author David Salter david@davidsalter.com
 */
@RunWith(Arquillian.class)
public class ToDoItemBeanValidationTest {

    @Inject
    Validator validator;

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(ToDoItem.class.getPackage())
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void validationFailsWhenDescriptionIsTooShort() throws Exception {
        ToDoItem item = new ToDoItem("1");

        Set<ConstraintViolation<ToDoItem>> violations = validator.validate(item);

        Assert.assertEquals("1 violation was found", 1, violations.size());
    }

    @Test
    public void validationFailsWhenDescriptionIsTooLong() throws Exception {
        ToDoItem item = new ToDoItem("123456789012345678901234567890123456789012345678901");

        Set<ConstraintViolation<ToDoItem>> violations = validator.validate(item);

        Assert.assertEquals("1 violation was found", 1, violations.size());
    }

    @Test
    public void validationSucceedsWhenDescriptionIsValid() throws Exception {
        ToDoItem item = new ToDoItem("1234567890");

        Set<ConstraintViolation<ToDoItem>> violations = validator.validate(item);

        Assert.assertEquals("0 violations were found", 0, violations.size());
    }
}
