/*
 * Copyright 2013 David Salter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.apps.todo.web;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.davidsalter.apps.todo.model.ToDoItem;
import com.davidsalter.apps.todo.ejb.ToDoItemController;
import java.util.Date;
import java.util.TimeZone;

/**
 * CDI bean responsible for managing the interaction between the web pages and 
 * the business model (implemented as a Stateless Session Bean).
 * 
 * @author David Salter david@davidsalter.com
 */
@Named
@SessionScoped
public class ToDoBean implements Serializable {

    @EJB
    ToDoItemController todoItemController;
    ToDoItem toDelete = null;
    ToDoItem item = null;
    Date lastRefresh = null;

    public ToDoBean() {
        item = new ToDoItem();
    }

    /**
     * Gets the current time so that dates can be displayed correctly within
     * web pages.
     * 
     * @return The current default timezone.
     */
    public TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }

    /**
     * Gets a list of To Do items.
     * 
     * @return List of To Do items.
     */
    public List<ToDoItem> getItems() {
        lastRefresh = new Date();
        return todoItemController.getItems();
    }

    /**
     * Gets the data and time that the list of To Do items was last
     * retrieved using the <code>ToDoBean.getItems()</code> method.
     * 
     * @return Last refresh date and time.
     */
    public Date getLastRefresh() {
        return lastRefresh;
    }

    /**
     * Sets an item to be deleted subject to confirmation.
     * 
     * @param item The item to be deleted.
     * 
     * @return The JSF return navigation.
     */
    public String deleteItem(ToDoItem item) {
        setToDelete(item);
        return "confirm";
    }

    /**
     * Sets an item to be edited for use in the edit page.
     * 
     * @param item The item to be edited.
     * 
     * @return The JSF return navigation. 
     */
    public String editItem(ToDoItem item) {
        setItem(item);
        return "edit";
    }

    /**
     * Adds a To Do item to the database.
     * 
     * @return The JSF return navigation.
     */
    public String addConfirmedItem() {
        todoItemController.addItem(item);
        item = new ToDoItem();
        return "items";
    }

    /**
     * Deletes a confirmed item.
     * 
     * @return The JSF return navigation.
     */
    public String deleteConfirmedItem() {
        todoItemController.deleteItem(toDelete);
        return "items";
    }

    /**
     * Edits a confirmed item.
     * 
     * @return The JSF return navigation.
     */
    public String editConfirmedItem() {
        todoItemController.editItem(item);
        item = new ToDoItem();
        return "items";
    }

    /**
     * Aborts the deletion of a confirmed item.
     * 
     * @return The JSF return navigation.
     */
    public String cancelDeleteConfirmedItem() {
        toDelete = null;
        return "items";
    }

    /**
     * Aborts the editing of a confirmed item.
     * 
     * @return The JSF return navigation.
     */
    public String cancelConfirmedItem() {
        item = new ToDoItem();
        return "items";
    }

    /**
     * Gets the currently selected item for adding / editing.
     * 
     * @return The selected item.
     */
    public ToDoItem getItem() {
        return item;
    }

    /**
     * Sets the current item for adding / editing.
     * 
     * @param item The item to add / edit.
     */
    public void setItem(ToDoItem item) {
        this.item = item;
    }

    /**
     * Gets the currently selected item for deleting.
     * 
     * @return The current item selected for deletion.
     */
    public ToDoItem getToDelete() {
        return toDelete;
    }

    /**
     * Sets the item to be deleted subject to confirmation.
     * 
     * @param item The item to be deleted.
     */
    public void setToDelete(ToDoItem item) {
        toDelete = item;
    }
}
