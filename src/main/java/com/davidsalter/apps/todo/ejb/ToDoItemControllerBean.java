/*
 * Copyright 2013 David Salter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.apps.todo.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.davidsalter.apps.todo.model.ToDoItem;

/**
 * JPA implementation of the ToDoItemController interface.
 * 
 * @author David Salter david@davidsalter.com
 */

@Stateless
public class ToDoItemControllerBean implements ToDoItemController {

    @PersistenceContext(name = "ToDo_PU")
    EntityManager em;

    /**
     * Get all of the To Do items stored in the database.
     * @return The To Do items.
     */
    public List<ToDoItem> getItems() {
        Query query = em.createQuery("select items from ToDoItem items order by items.description asc");
        return query.getResultList();
    }

    /**
     * Get a specific To do item from the database.
     * @param id The id of the To Do item to retrieve. 
     * @return 
     */
    public ToDoItem getItem(Integer id) {
        return em.find(ToDoItem.class, id);
    }
    
    /**
     * Add a To Do item to the database.
     * @param item The item to add.
     */
    public void addItem(ToDoItem item) {
        em.persist(item);
    }

    /**
     * Delete a To Do item from the database.
     * @param item The item to delete.
     */
    public void deleteItem(ToDoItem item) {
        em.remove(em.merge(item));
    }

    /**
     * Update a To Do item in the database.
     * @param item The item to update.
     */
    public void editItem(ToDoItem item) {
        em.persist(em.merge(item));
    }
}
