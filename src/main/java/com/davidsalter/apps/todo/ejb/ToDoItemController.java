/*
 * Copyright 2013 David Salter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.apps.todo.ejb;

import java.util.List;

import javax.ejb.Local;

import com.davidsalter.apps.todo.model.ToDoItem;

/**
 * Local interface defining methods for the ToDoItemControllerBean.
 * 
 * @author David Salter david@davidsalter.com
 */
@Local
public interface ToDoItemController {

    /**
     * Get all of the To Do items.
     * @return List of To Do items
     */
    List<ToDoItem> getItems();

    /**
     * Get a specific To Do item based upon it's id.
     * @param id The id of the To Do item to retrieve
     * @return  The To Do item.
     */
    ToDoItem getItem(Integer id);
    
    /**
     * Add a new To Do item.
     * @param item The item to add
     */
    void addItem(ToDoItem item);

    /**
     * Delete a To Do item.
     * @param item The To Do item to delete.
     */
    void deleteItem(ToDoItem item);

    /**
     * Update a To Do item.
     * @param item The item to update.
     */
    void editItem(ToDoItem item);
}
