# ToDoEE

ToDoEE is an example Java EE 6 application designed to be deployed to Apache TomEE.

The application uses an HSQL database to store To Do items.  The database is configured in the web.xml file to store data under the <tomee_home>/data directory.  If you wish to use a different database, ensure is correctly configured before running the application.

    <data-source>
      <name>ToDoDataSource</name>
      <class-name>org.hsqldb.jdbcDriver</class-name>
      <url>jdbc:hsqldb:file:../data/hsqldb/tododb</url>
      <user>sa</user>
      <password></password>
    </data-source>

#### To run the application:

    mvn tomee:run
  
#### To run the application tests:
  
    mvn test
